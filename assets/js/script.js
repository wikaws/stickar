var modelViewer = document.querySelector('model-viewer');
var progressBar = modelViewer ? modelViewer.querySelector('.progress-bar') : null;
var updatingBar = modelViewer ? modelViewer.querySelector('.update-bar') : null;
var updatingText = modelViewer ? modelViewer.querySelector('.update-text') : null;
var dialog = document.querySelector('.dialog');
var dialogNext = document.querySelector('.dialog.dialog-next');
var howToBtn = document.querySelector('.howTo');
var dialogCloseBtn = document.querySelector('.dialog-close-btn');
var dialogNextBtn = document.querySelector('.dialog-next-btn');
var arButton = document.querySelector('#buttonAR');
var scaleButton = document.querySelector('#btnScale');
var scaleResetButton = document.querySelector('#btnResetScale');
var positionYButton = document.querySelector('#btnYPosition');
var prevButton = document.querySelector('.c-nav-prev');
var nextButton = document.querySelector('.c-nav-next');
var urlParams = new URLSearchParams(window.location.search);
var object3D = urlParams.get('type');
var colorHex = urlParams.get('colorHex');
var slideIndex = { value: 0 };
var cameraTarget, minCamOrbit, maxCamOrbit, filePathGLB;
var percentage = 0;
var touchStartX = 0;
var touchEndX = 0;
var material;

const onProgress = (event) => {
  updatingBar.style.width = `${event.detail.totalProgress * 100}%`;
  if (event.detail.totalProgress === 1) {
    progressBar.classList.add('hide');
    event.target.removeEventListener('progress', onProgress);
  } else {
    progressBar.classList.remove('hide');
  }
};
// document.querySelector('model-viewer').addEventListener('progress', onProgress);
modelViewer.addEventListener('progress', function(event) {
  if (event.detail && event.detail.totalProgress) {
    percentage = Math.round(event.detail.totalProgress * 100);
    updatingText.innerHTML = `Loading Asset ${percentage}%`;
    updatingBar.style.width = `${percentage}%`;
  }

  if (percentage === 100) {
    progressBar.classList.add('hide');
    modelViewer.removeEventListener('progress', onProgress);
    document.querySelector(".c-heading a").classList.remove("hidden");
    document.querySelector(".c-button.markerless a").classList.remove("disabled");
    document.querySelector(".c-button.markerless a").classList.add("bg-bluevw");
    document.querySelector(".c-button.markerbased a").classList.remove("disabled");
    document.querySelector(".c-button.markerbased a").classList.add("bg-bluevw");
  }
});


function isMobile() {
  const screenWidth = window.screen.width;
  const screenHeight = window.screen.height;
  const isAndroid = /Android/i.test(navigator.userAgent);
  const isIpad = /iPad/i.test(navigator.userAgent);

  const isTabletWidth = screenWidth > 600 && screenWidth < 1024;
  const isMobileWidth = screenWidth <= 600;

  return (isAndroid && !isIpad && isTabletWidth) || (isMobileWidth);
}

function isTabletPortrait() {
  const userAgent = navigator.userAgent.toLowerCase();
  const isTablet = /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(userAgent);
  const screenWidth = window.screen.width;
  const screenHeight = window.screen.height;

  return isTablet && window.matchMedia("(orientation: portrait)").matches;
}

if(modelViewer) {

  if (object3D) {

  switch(object3D) {
    case 'blauderkafer':
      object3D = "SuperBeetle"
      break;
    case 'vw_beetle':
      object3D = "Beetle"
      break;
    case 'vw_combi':
      object3D = "Combi"
      break;
    case 'vw_dakota':
      object3D = "Dakota"
      break;
    case 'vw_type181':
      object3D = "Safari"
      break;
    default:
      object3D = "Beetle"
  }

    filePathGLB = `assets/models/${object3D}.glb`
    document.querySelector("a.c-button.back").href = `/?type=${object3D}`;
    document.querySelector(".c-title span").innerHTML = 'Volkswagen';
    document.querySelector(".c-title .c-heading h1").innerHTML = object3D;
    modelViewer.setAttribute('poster', `assets/images/poster-${object3D}.webp`);
    // modelViewer.setAttribute('ios-src', `assets/models/${object3D}.usdz`);
  } else {
    filePathGLB = 'assets/models/Beetle.glb'
    document.querySelector("a.c-button.back").href = '/?type=vw_beetle';
    document.querySelector(".c-title span").innerHTML = 'Volkswagen';
    modelViewer.setAttribute('poster', 'assets/images/poster-Beetle.webp');
    // modelViewer.setAttribute('ios-src', 'assets/models/Beetle.usdz');
  }

  progressBar.classList.remove('hide');

  modelViewer.setAttribute('src', filePathGLB);

  modelViewer.addEventListener('load', () => {
      console.log(colorHex);
      if(colorHex){
        switch(object3D) {
          case 'SuperBeetle':
            material = modelViewer.model.materials[0];
            break;
          case 'Beetle':
            material = modelViewer.model.materials[0];
            break;
          case 'Combi':
            material = modelViewer.model.materials[4];
            break;
          case 'Dakota':
            material = modelViewer.model.materials[5];
            break;
          case 'Safari':
            material = modelViewer.model.materials[4];
            break;
        }
      }
      console.log(material);
      if (material != null) {
        material.pbrMetallicRoughness.setBaseColorFactor("#"+colorHex);
        material.pbrMetallicRoughness.setMetallicFactor("0.04");
        material.pbrMetallicRoughness.setRoughnessFactor("0.05");
      }

      // if (modelViewer.canActivateAR) {
      //   modelViewer.activateAR();
      // } else {
      //   console.error('AR mode is not supported.');
      // }

      // const whichMaterial = (event) => {
      //   console.log(modelViewer.materialFromPoint(event.clientX, event.clientY));
      // };

      // modelViewer.addEventListener("click", whichMaterial);
  });

  const handleARButtonClick = () => {
    if (modelViewer.canActivateAR) {
      modelViewer.activateAR();
    } else {
      console.error('AR mode is not supported.');
      alert('AR mode is not supported on your device');
    }
  };

  arButton.addEventListener('click', handleARButtonClick)
  document.querySelector("#exitAR").addEventListener('click', async () => {
    document.querySelector(".c-button-wrapper.c-button-scale").style.display = 'none'
  });

}

const handleContextMenu = (e) => {
  e.preventDefault();
};

document.onkeydown = (e) => {
  const forbiddenKeys = ['F12', 'C', 'I', 'J', 'U'];
  if (forbiddenKeys.includes(e.code) || (e.ctrlKey && e.shiftKey)) {
    e.preventDefault();
  }
};

const showHowToDialog = () => {
  dialog.classList.remove('hidden');
};

const showHowToDialogNext = () => {
  if(!dialog.classList.contains('hidden')){
    dialog.classList.add('hidden');
  }
  dialogNext.classList.remove('hidden');
};

const closeHowToDialog = () => {
  if(!dialog.classList.contains('hidden')){
    dialog.classList.add('hidden');
  }
  if(dialogNext){
    if(!dialogNext.classList.contains('hidden')){
      dialogNext.classList.add('hidden');
    }
  }
};

if(dialogNext){
  dialogNextBtn.addEventListener('click', showHowToDialogNext);
}

howToBtn.addEventListener('click', showHowToDialog);
dialogCloseBtn.addEventListener('click', closeHowToDialog);
