{
  "type": "getItems",
  "data": [
    {
      "id": "5c133800-8d73-4ffe-abd3-9876657f9752",
      "name": "Book",
      "storage_id": "1",
      "sub_storage": "2",
      "total": "2"
    },
    {
      "id": "4e5c1d92-473d-4b9b-9fff-74f8e45fbaa8",
      "name": "Cable",
      "storage_id": "1",
      "sub_storage": "3",
      "total": "2"
    },
    {
      "id": "2c1eccc2-1f31-4f2a-89e9-d81226ba477f",
      "name": "Book",
      "storage_id": "1",
      "sub_storage": "2",
      "total": "10"
    },
    {
      "id": "85b572dd-ba76-4557-8ed0-96cd2de515f9",
      "name": "Pencil",
      "storage_id": "1",
      "sub_storage": "1",
      "total": "10"
    },
    {
      "id": "31f6d115-7698-4d83-921d-b4d4454ca329",
      "name": "Poster",
      "storage_id": "2",
      "sub_storage": "2",
      "total": "1"
    },
    {
      "id": "628a4097-18bb-47c9-93f1-129be0c84074",
      "name": "Cup",
      "storage_id": "2",
      "sub_storage": "3",
      "total": "6"
    },
    {
      "id": "3e0217d1-23d6-47d8-b127-004bee031742",
      "name": "USB",
      "storage_id": "1",
      "sub_storage": "4",
      "total": "2"
    },
    {
      "id": "83846c91-8e08-4af6-bb37-17bac5c61c1c",
      "name": "Frame",
      "storage_id": "2",
      "sub_storage": "1",
      "total": "5"
    }
  ]
}